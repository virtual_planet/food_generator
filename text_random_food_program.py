import requests
import pprint
fridge_ingredients = ['rice', 'chilli']
recipe_id = []
program_status = True
current_ingredients = ""
payload = {
    'apiKey': 'b88151dbbe474f3c9b6ccabe931126f2',
    'includeIngredients': fridge_ingredients,
    'ignorepantry': True,
    'intolerances': ['Peanut', 'Sesame', 'Tree Nut'],
    'number': 5
}

specific_id = {
    'apiKey': 'b88151dbbe474f3c9b6ccabe931126f2',
    'ids': recipe_id
}

head = {
    'Content-Type': 'application/json',
}

print('Random Food Generator\n')

def main_menu():
    print(f'\n')
    print('1. Display current ingredients') 
    print('2. Add ingredients') 
    print('3. Delete ingredients')
    print('4. Search for recipes')
    print('5. Recipes via IDs')
    answer = input(f'\nPlease select an option: \n\n')
    if answer == str(1):
        display_ingredients()
        main_menu()
    elif answer == str(2):
        add_ingredients()
        main_menu()
    elif answer == str(3):
        delete_ingredients()
        main_menu()
    elif answer == str(4):
        search_recipes()
        main_menu()
    elif answer == str(5):
        recipe_id()
        main_menu()
    else:
        print(f'{answer} is not a valid option, please try again.')
        main_menu()

#Displays the current ingredients in the fridge, after displaying the program puts the user back into the main menu
def display_ingredients():
    print('\nThe current items in your fridge are as follows: \n')
    for v in fridge_ingredients:
        print(f'- {v.title()}')
    print(f'\n')

#Requests the user to type in the ingredients in their fridge until the type in the word "stop" which then breaks out of the loop as the while loop condition is no longer met.
def add_ingredients():
    global program_status
    while program_status == True:
        current_ingredients = input(f'\nPlease tell me the current ingredients in your fridge. Once you have finished type in "stop" finialise your selection: ')
        if current_ingredients.lower() != 'stop':
            fridge_ingredients.append(current_ingredients)
        elif current_ingredients.lower() == 'stop':
            break

#Requests the user to type in the ingredients that they wish to remove from the list, if the item doesn't isn't a except error will be displayed.
def delete_ingredients():
    while program_status == True:
        answer = input('Please confirm the ingredients you wish to remove from the list, once you have finished type in "stop" to finialise your selection:')
        if answer.lower() != 'stop':
            try:
                fridge_ingredients.remove(answer.lower())
                print(f'{answer.lower()} has been removed.')
            except ValueError:
                print(f'{answer.lower()} is not in the list.')
        elif answer.lower() == 'stop':
            break

#Searches for a recipe using the parameters provided via the payload dictionary, the results will be provided in json format. 
def search_recipes():
    url = requests.get('https://api.spoonacular.com/recipes/complexSearch', headers=head, params=payload)
    j = url.json()
    for v in j['results']:
        print(f'\n\t{v["title"]}: Recipe ID: {v["id"]}: Picture: {v["image"]}')



#Prompts the user to enter a recipe ID, the ID is then fed into the answer variable which is used to retrieve the recipe assoicated with that ID.
def recipe_id():
    answer = input('Please enter a recipe ID: ')
    url = requests.get(f'https://api.spoonacular.com/recipes/{answer}/information', headers=head, params=payload)
    j = url.json()
    pprint.pprint(j)

main_menu()
